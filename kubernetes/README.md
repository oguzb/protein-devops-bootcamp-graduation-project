# **Deploy the Application to Kubernetes**

In this chapter of the project, We will create GitLab CI/CD to deploy the application to kubernetes cluster.

This project contains 1 deployment and 1 service.

This directory contains main required manifest files to create deployment and service. GitLab CI/CD yaml file is in main directory of the project.

## **Table of Contents**
---

- [Directories](#directories)

- [Files](#files)

- [Archirecture](#architecture)

- [Run](#run)

- [Technologies](#technologies)

## **Directories**
---
- **images/** : It contains screenshots for readme file

## **Files**
---
- **deployment.yaml**: Manifest file to create deployment.
- **service.yaml**: Manifest file to create load balancer service.


## **Architecture**
---

**This part of the project contains 3 structure.**

**NOTE**: Helm must be installed.You can check out instruction of installation from [here](https://helm.sh/docs/intro/install/).

### 1. **GitLab Agent**
---   
  To connect a Kubernetes cluster to GitLab, we must install an agent in our cluster.

  To install agent to the cluster;
  - Select project then Left Menu > Infrastructure > Kubernetes clusters > Connect a cluster

  Select an agent or write a name to create new.Then, click Register.

![Select an agent or write a name to create](images/select_or_create_agent.png)

  Then you will see a page like below. The page contains instructions to install an agent with helm to the cluster.

![Installation instructions](images/install_agent_with_helm.png)

We need to run these commands to install the agent to the cluster.

After run the commands, you can see the agent from in "Left Menu > Infrastructure > Kubernetes clusters".

![Agent list](images/agent_list.png)

**If you want to more information about agent installation, you can check out [here](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html).**


### 2.  **.gitlab-ci.yml file** 
---
We need to write kubernetes part to gitlab-ci.yml file. We have a "gitlab-ci.yml" file in main directory of the project. Stage "deploy-app-to-kubernetes" is for kubernetes.

This gitlab-ci.yml does the following, in order;

- **"run-tests" stage**: It runs some tests. (It just write to stdout somethings.)
- **"push-image-to-docker-hub" stage"**: Firstly it runs "docker login" command. Then build the image and push it to docker hub.
- **"deploy-app-to-ecs" stage**: It updates the ECS Fargate service using "awscli".
- **"deploy-app-to-kubernetes" stage**: It updates kubernetes cluster. 

### 3. ** Manifest files**
---
Required manifest files.

 

## **Run**
---

- To create deployment and service run these commands;
  - `kubectl apply -f deployment.yaml` : This command creates the deployment.
  - `kubectl apply -f service.yaml` : This command creates the deployment. 

    After run the commands, we can access the application **http://IP:30058**

- If someone send the project repository a new commit, gitlab-ci.yml will update the deployment automatically. Then Kubernetes Cluster will be updated.


## **Technologies**
---
- Kubernetes
- Minikube
- VirtualBox (for test it local)
- GitLab CI/CD