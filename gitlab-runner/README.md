# **Create Specific GitLab Runner with Terraform in AWS EC2 Instance**

## **Table of Contents**
---
- [Requirements](#requirements)

- [Files](#files)

- [What it does?](#what-it-does)

- [Installation Instructions](#installation-instructions)

- [Technologies](#technologies)


## **Requirements**
---
-  **Terraform** must be installed.You can find instructions of installation of Terraform from [here](https://learn.hashicorp.com/tutorials/terraform/install-cli).
-  Create public and private keygen.
  
  `ssh-keygen -t rsa -f file_name`

- Some variables must be defined to vars.tf file.
    - **AWS_ACCESS_KEY** : Acess key  of AWS user
    - **AWS_SECRET_KEY** : Secret key of AWS user
    - **AWS_REGION** : AWS region which resources will be created.
    - **PATH_TO_PRIVATE_KEY** : Private ssh key path. We use it to access the EC2 instance remotely.
    - **PATH_TO_PUBLIC_KEY** : Public ssh key path.
    - **INSTANCE_USERNAME** : EC2 instance username.
    - **GITLAB_URL** : URL to register the runner.
    - **GITLAB_RUNNER_REG_TOKEN** : GitLab registration token.
      - **NOTE:** You can find URL and registration token from GitLab Left Menu > Settings > CI/CD > Runners
    - **GITLAB_RUNNER_DESCRIPTION** : Description of the runner.
    - **GITLAB_RUNNER_EXECUTOR**: Executer of the runner.
    - **GITLAB_RUNNER_TAG_LIST**: Tags. 
    - **KEY_NAME**: AWS Key Pair name.
    - **SG_NAME**: AWS Security Group name

## **Files**
---
- **keys/**
  - **mykey**: Private key file
  - **mykey.pub**: Public key file
- **scripts/**
  - **installation.sh**: It install gitlab-runner, docker and awscli.
- **instance.tf**: It contains terraform codes to create EC2 instance, security group and key pair.
- **provider.tf**: It contains terraform provider informations.
- **vars.tf**: It contains variables.
- **versions.tf**: It contains terraform required version informations.
---

## **What it does?**

- It creates an AWS Key Pair with public keys which we created.
- It creates an AWS Security Group.
- It creates an AWS EC2 Instance. We can connect the instance with key pair which we created.
- Copy "script/installation.sh" file to "/tmp/installation.sh".
- Give execution permission to the script.
- Run the script.The script will install gitlab-runner, docker and awscli.
- Run gitlab-runner register command.


## **Installation Instructions**

- `terraform init` : It used to initialize a working directory containing Terraform configuration files.
- **Optional**:
    - `terraform plan` : It creates an execution plan, which lets you preview the changes that Terraform plans to make to your infrastructure.
    - `terraform fmt` : It is used to rewrite Terraform configuration files to a canonical format and style.
- `terraform apply` : It executes the actions proposed in a Terraform plan.
  **If everything is configured correctly, you can see the runner in GitLab Left Menu > Settings > CI/CD > Runners > Specific runners.**

## **Technologies**
---
- Linux
- Amazon Web Services
- Shell Scripting
- Terraform
