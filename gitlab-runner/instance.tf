resource "aws_key_pair" "mykey" {
  key_name   = var.KEY_NAME
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}


resource "aws_security_group" "runner_security_group" {

  name        = var.SG_NAME

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_instance" "gitlab_runner_server" {
  ami             = var.AMIS[var.AWS_REGION]
  instance_type   = "t2.micro"
  key_name        = aws_key_pair.mykey.key_name
  security_groups = ["${aws_security_group.runner_security_group.name}"]

  provisioner "file" {
    source      = "scripts/installation.sh"
    destination = "/tmp/installation.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/installation.sh",
      "sudo sed -i -e 's/\r$//' /tmp/script.sh",
      "sudo chmod +x /tmp/installation.sh",
      "sudo /tmp/installation.sh",
      "sudo chmod 666 /var/run/docker.sock",
      "sudo gitlab-runner register --non-interactive --url '${var.GITLAB_RUNNER_URL}' --registration-token '${var.GITLAB_RUNNER_REG_TOKEN}' --description '${var.GITLAB_RUNNER_DESCRIPTION}' --executor '${var.GITLAB_RUNNER_EXECUTOR}' --tag-list '${var.GITLAB_RUNNER_TAG_LIST}'"
    ]
  }

  connection {
    host        = coalesce(self.public_ip, self.private_ip)
    type        = "ssh"
    user        = var.INSTANCE_USERNAME
    private_key = file(var.PATH_TO_PRIVATE_KEY)
  }

}
