#! /bin/bash
#######################
## Author : Oguz BALKAYA <oguz.balkaya@gmail.com>
## Description : This script will install gitlab runner, awscli and docker engine
## Date : 10-07-2022
## Version : 1.0
#######################

sudo apt update

#GitLab Runner Installation.
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner -y

#Docker Installation.

sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
apt-cache policy docker-ce -y
sudo apt install docker-ce -y
sudo systemctl enable docker
sudo systemctl start docker

#aswcli Installation.

sudo apt install awscli -y