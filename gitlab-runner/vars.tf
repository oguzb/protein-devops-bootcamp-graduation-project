variable "AWS_ACCESS_KEY" {
  type    = string
  default = "AKIA5QWK2G56JJHLA4FY"
  description = "Access key of AWS user"
  #https://docs.aws.amazon.com/powershell/latest/userguide/pstools-appendix-sign-up.html
}


variable "AWS_SECRET_KEY" {
  type    = string
  default = "57H4IRGEE2JEf02qYG514nnBshfrUBjl6m4jDSWi"
  description = "Secret key of AWS user"
  #https://docs.aws.amazon.com/powershell/latest/userguide/pstools-appendix-sign-up.html
}

variable "AWS_REGION" {
  type    = string
  default = "eu-central-1"
  description = "Region where resources will be created"
}

variable "AMIS" {
  type = map
  default = {
    eu-central-1 = "ami-065deacbcaac64cf2"
  }
}

variable "PATH_TO_PRIVATE_KEY" {
  type = string
  default = "keys/mykey"
  description = "Private ssh key path. We use it to access the EC2 instance remotely."
}

variable "PATH_TO_PUBLIC_KEY" {
  type = string
  default = "keys/mykey.pub"
  description = "Public ssh key path."
}

variable "INSTANCE_USERNAME" {
  type = string
  default = "ubuntu"
  description = "EC2 instance username."
}

variable "GITLAB_RUNNER_URL" {
  type = string
  default = "https://gitlab.com/"
  description = "URL to register the runner."
}

variable "GITLAB_RUNNER_REG_TOKEN" {
  type = string
  default = "GR1348941y9s9qQZero_3Kocqoe_x"
  description = "GitLab registration token."
}

variable "GITLAB_RUNNER_DESCRIPTION" {
  type = string
  default = "Protein DevOps Bootcamp Graduation Project Runner"
  description = "Description of the runner."
}

variable "GITLAB_RUNNER_EXECUTOR" {
  type = string
  default = "shell"
  description = "Executer of the runner."
}

variable "GITLAB_RUNNER_TAG_LIST" {
  type = string
  default = "docker,node,react,aws"
  description = "Runner tags"
}

variable "KEY_NAME" {
  type = string
  default = "mykey"
  description = "AWS Key Pair name"
}

variable "SG_NAME" {
  type = string
  default = "GitLabRunner_SecurityGroup"
  description = "AWS Security Group name"
}