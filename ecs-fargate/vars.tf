variable "AWS_ACCESS_KEY" {
  type        = string
  default     = "AKIA5QWK2G56JJHLA4FY"
  description = "ACCESS KEY of AWS USER"
  #https://docs.aws.amazon.com/powershell/latest/userguide/pstools-appendix-sign-up.html
}


variable "AWS_SECRET_KEY" {
  type        = string
  default     = "57H4IRGEE2JEf02qYG514nnBshfrUBjl6m4jDSWi"
  description = "SECRET KEY of AWS USER"
  #https://docs.aws.amazon.com/powershell/latest/userguide/pstools-appendix-sign-up.html
}

variable "AWS_REGION" {
  type        = string
  default     = "eu-central-1"
  description = "Region where resources will be created"
}

variable "AVAILABILITY_ZONES" {
  type        = list(any)
  default     = ["eu-central-1a", "eu-central-1b"]
  description = "Availability zones which the region has"
}

variable "CIDR_BLOCK" {
  type        = string
  default     = "10.0.0.0/16"
  description = "CIDR Block of VPC"
}

variable "PRIVATE_SUBNETS" {
  type        = list(any)
  default     = ["10.0.0.0/20", "10.0.32.0/20"]
  description = "Private subnets. 1 private subnet for each az."
}

variable "PUBLIC_SUBNETS" {
  type        = list(any)
  default     = ["10.0.16.0/20", "10.0.48.0/20"]
  description = "Public subnets. 1 public subnet for each az."
}

variable "CONTAINER_PORT" {
  type    = string
  default = "80"
}

variable "HOST_PORT" {
  type    = string
  default = "80"
}

variable "CLUSTER_CPU" {
  type    = number
  default = 256
}

variable "CLUSTER_MEMORY" {
  type    = number
  default = 512
}

variable "APP_IMAGE" {
  type        = string
  default     = "oguzbalkaya/my_project:latest"
  description = "Image and tag of the application"
}

variable "SERVICE_DESIRED_COUNT" {
  type    = number
  default = 1
}

variable "SERVICE_MIN_HEALTHY_PERCENT" {
  type    = number
  default = 50
}

variable "SERVICE_MAX_HEALTHY_PERCENT" {
  type    = number
  default = 100
}

variable "TG_HC_THRESHOLD" {
  type    = string
  default = "3"
}

variable "TG_HC_INTERVAL" {
  type    = string
  default = "30"
}

variable "TG_HC_MATCHER" {
  type    = string
  default = "200"
}

variable "TG_HC_TIMEOUT" {
  type    = string
  default = "3"
}

variable "TG_HC_PATH" {
  type    = string
  default = "/"
}

variable "TG_HC_UNHEALTHY_THRESHOLD" {
  type    = string
  default = "2"
}

variable "AS_MAX_CAPACITY" {
  type    = number
  default = 4
}

variable "AS_MIN_CAPACITY" {
  type    = number
  default = 1
}

variable "AS_TARGET_MEMORY_VALUE" {
  type    = number
  default = 80
}

variable "AS_TARGET_CPU_VALUE" {
  type    = number
  default = 60
}

variable "SG_ALB_NAME" {
  type    = string
  default = "SecurityGroup-ALB"
}

variable "SG_ECS_NAME" {
  type    = string
  default = "SecurityGroup-ECS"
}

variable "CONTAINER_NAME" {
  type    = string
  default = "ReactApp-container"
}

variable "CLUSTER_NAME" {
  type    = string
  default = "ReactApp-Cluster"
}

variable "IAM_TASK_ROLE_NAME_ECS" {
  type    = string
  default = "ECS_TASK_ROLE"
}

variable "IAM_TASK_ROLE_NAME_ECS_EXE" {
  type    = string
  default = "ECS_TASK_EXE_ROLE"
}

variable "SERVICE_NAME" {
  type    = string
  default = "ReactApp-service"
}

variable "LB_NAME" {
  type    = string
  default = "ReactApp-LoadBalancer"
}

variable "LB_TG_NAME" {
  type    = string
  default = "ReactApp-TargetGroup"
}

variable "AGS_CPU_UP_NAME" {
  type    = string
  default = "AutoScaling-CPU_UP"
}

variable "AGS_CPU_DOWN_NAME" {
  type    = string
  default = "AutoScaling-CPU_DOWN"
}

variable "AS_CPU_LOW" {
  type    = string
  default = "20"
}

variable "AS_CPU_HIGH" {
  type    = string
  default = "50"
}

variable "AS_CPU_UP_NAME" {
  type    = string
  default = "ReactApp_CPU_UP"
}

variable "AS_CPU_DOWN_NAME" {
  type    = string
  default = "ReactApp_CPU_DOWN"
}

variable "CW_HIGH_ALARM_NAME" {
  type    = string
  default = "ECS-CPU_Utilization_High"
}

variable "CW_LOW_ALARM_NAME" {
  type    = string
  default = "ECS-CPU_Utilization_Low"
}

variable "CW_LOG_GROUP_NAME" {
  type    = string
  default = "ecs-reactapp"
}

variable "CW_LOG_STREAM_NAME" {
  type    = string
  default = "ecs-log-stream"
}

variable "CW_RETENTION_DAYS" {
  type    = number
  default = 30
}

variable "TAG_NAME" {
  type    = string
  default = "ReactApp"
}