data "template_file" "ecs_task_role" {
  template = file("./templates/ecs_task_role.json")
}

data "template_file" "ecs_task_exe_role" {
  template = file("./templates/ecs_task_execution_role.json")
}

resource "aws_iam_role" "ecs_task_role" {
  name               = var.IAM_TASK_ROLE_NAME_ECS
  assume_role_policy = data.template_file.ecs_task_role.rendered

  tags = {
    Name = var.TAG_NAME
  }
}

resource "aws_iam_role" "ecs_task_exe_role" {
  name               = var.IAM_TASK_ROLE_NAME_ECS_EXE
  assume_role_policy = data.template_file.ecs_task_exe_role.rendered

  tags = {
    Name = var.TAG_NAME
  }
}

resource "aws_iam_role_policy_attachment" "ecs_task_exe_role_policy_attachment" {
  role       = aws_iam_role.ecs_task_exe_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}