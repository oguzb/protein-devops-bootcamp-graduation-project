resource "aws_security_group" "SG_ALB" {
  name   = var.SG_ALB_NAME
  vpc_id = aws_vpc.MAIN_VPC.id

  ingress {
    protocol    = "tcp"
    from_port   = var.CONTAINER_PORT
    to_port     = var.CONTAINER_PORT
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.TAG_NAME
  }

}

resource "aws_security_group" "SG_ECS" {
  name   = var.SG_ECS_NAME
  vpc_id = aws_vpc.MAIN_VPC.id

  ingress {
    protocol    = "tcp"
    from_port   = var.HOST_PORT
    to_port     = var.HOST_PORT
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.TAG_NAME
  }
}
