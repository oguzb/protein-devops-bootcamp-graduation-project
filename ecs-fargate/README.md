# **Deploy the Application to AWS ECS Fargate Using Terraform**

This directory contains terraform script stack to deploy the application to AWS Fargate. 


![ECS Definition](./images/ecs_definition.png)

If you want to create an ECS Fargate manually, AWS can build everything (VPC, Load Balancer etc.) for us. After creating it manually, AWS will show us the services that created as below.

![Manuel ECS Progress](./images/ecs_progress.png)
 
If you want to create AWS ECS Fargate with Terraform, you need to create the services one by one with terraform.Also, you need to associate the services.

## Project Architecture
---

**NOTE:** I followed a post from AWS Blog while creating the architecture. You can read the post from [here](https://aws.amazon.com/tr/blogs/compute/task-networking-in-aws-fargate/).

![ECS Architecture](images/ecs.png)

- **AWS Provider** : Firstly, we need to use terraform aws provider. Provider definition is in "provider.tf". We need to give access key, secret key and region informations.

- **Network (VPC, Routet table etc.)**: 
  - We need to create a VPC and add subnets. We need 1 private and 1 public subnet for each availability zones. We have 2 availability zones in region.So we need 2 public and 2 private subnets.

  - We need to create routing table for the public subnet, going through the internet gateway.

  - For communication of private subnets with the outside world, we need to attach NAT gateways which also need an Elastic IP associated.
  
  - We need to create the route table for the private subnet, where traffic is routed through the NAT gateway.

  **The above operations are defined in file "vpc.tf".After these operations, we will have a new VPC.We can use it in the cluster.**

- **Security Groups**:
  - We will create a security group for load balancer. It allows only access via TCP port 80.
  - We will create another security group for ECS tasks.It allows ingres access only to the port that is exposed by the task.

  **The above operations are defined in file "security_groups.tf". After these operations, we will have 2 new security groups.**

- **Application Load Balancer**:
  - We need to a load balancer and a http listener.(We can also create https listener.But we need certificate for this.). Http listener funnels traffic to the target group.
  
  **The above operations are defined in file "load_balancer.tf".After these operations, we will have a load balancer, a target group and a http listener.**

- **IAM Role**
  - We have to give our task a task role.What AWS services does the task has access to?
  - We need to another role.It is task execution role.We need it because the task will be executed "serverless".
  
  **The above operations are defined in file "iam_roles.tf".After these operations, we will have 2 roles.**

- **Cluster**:
  - We need to create a cluster.Then we need to create a task definition. 
  - After that, we need to create an ecs service. ECS Service says how many of tasks should run in parallel, and makes sure that here always are enough health tals running.

  **The above operations are defined in file "ecs.tf".After these operations, we will have 2 roles.**

- **Autoscaling, CloudWatch Alarms and Log Group**:
  - We will create an autoscaling target, 2 policies. Also we will create CloudWatch alarms and a log group.
  - We will configure auto-scaling onto the Fargate instance.Scale up when CPU is above %50 and scale-down when CPU is below %20.
  
**The above operations are defined in files "autoscaling.tf" and "cloudwatch.tf".**




## **Directories and Files**
---

- **templates/** : It contains template files.
    - **ecs_task_execution_role.json**: It contains ECS Task Execution role.
    - **ecs_task_role.json**: It contains ECS TASK role.
    - **image.json**: It contains image definitions.
- **autoscaling.tf**: It contains Auto Scaling resources.
- **cloudwatch.tf**: It contains CloudWatch Log Group and Alarms.
- **ecs.tf**: It contains ECS resources and associations.
- **iam_roles.tf**: It contains IAM Role resources. They take the roles from files that in templates directory.
- **load_balancer.tf**: It contains Load Balancer and Listener definitions.
- **provider.tf**: It contains provider informations.
- **security_groups.tf**: It contains security group definitions.
- **vars.tf**: It contains variables.
- **versions.tf**: It contains version informations.
- **vpc.tf**: It contains VPC, Subnet, Route Table and Internet Gateway definitions.


## **Installation Instructions**

- `terraform init` : It used to initialize a working directory containing Terraform configuration files.
- **Optional**:
    - `terraform plan` : It creates an execution plan, which lets you preview the changes that Terraform plans to make to your infrastructure.
    - `terraform fmt` : It is used to rewrite Terraform configuration files to a canonical format and style.
- `terraform apply` : It executes the actions proposed in a Terraform plan.

## **Technologies**
---
- Terraform
- ECS Fargate
- AWS Auto Scaling
- AWS CloudWatch
- AWS Load Balancer
- AWS IAM
- AWS VPC
- AWS NatGateway
- AWS Security Group
- AWS Route Table
- AWS Target Group
- JSON
