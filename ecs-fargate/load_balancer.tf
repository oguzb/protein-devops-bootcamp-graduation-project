resource "aws_lb" "main" {
  name                       = var.LB_NAME
  internal                   = false
  load_balancer_type         = "application"
  security_groups            = [aws_security_group.SG_ALB.id]
  subnets                    = aws_subnet.public.*.id
  enable_deletion_protection = false

  tags = {
    Name = var.TAG_NAME
  }
}

resource "aws_lb_target_group" "main" {
  name        = var.LB_TG_NAME
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_vpc.MAIN_VPC.id

  health_check {
    healthy_threshold   = var.TG_HC_THRESHOLD
    interval            = var.TG_HC_INTERVAL
    protocol            = "HTTP"
    matcher             = var.TG_HC_MATCHER
    timeout             = var.TG_HC_TIMEOUT
    path                = var.TG_HC_PATH
    unhealthy_threshold = var.TG_HC_UNHEALTHY_THRESHOLD
  }

  tags = {
    Name = var.TAG_NAME
  }

}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.main.id
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.main.arn
  }

  tags = {
    Name = var.TAG_NAME
  }
}