#####Alarms
resource "aws_cloudwatch_metric_alarm" "high_cpu_utilization" {
  alarm_name          = var.CW_HIGH_ALARM_NAME
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = var.AS_CPU_HIGH

  dimensions = {
    ClusterName = aws_ecs_cluster.main.name
    ServiceName = aws_ecs_service.main.name
  }

  alarm_actions = [aws_appautoscaling_policy.scale_up.arn]


  tags = {
    Name = var.TAG_NAME
  }

}

resource "aws_cloudwatch_metric_alarm" "low_cpu_utilization" {
  alarm_name          = var.CW_LOW_ALARM_NAME
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = var.AS_CPU_LOW

  dimensions = {
    ClusterName = aws_ecs_cluster.main.name
    ServiceName = aws_ecs_service.main.name
  }

  alarm_actions = [aws_appautoscaling_policy.scale_down.arn]

  tags = {
    Name = var.TAG_NAME
  }

}

#####

#####Log group

resource "aws_cloudwatch_log_group" "reactapp_log_group" {
  name              = var.CW_LOG_GROUP_NAME
  retention_in_days = var.CW_RETENTION_DAYS

  tags = {
    Name = var.TAG_NAME
  }
}

resource "aws_cloudwatch_log_stream" "reactapp_log_stream" {
  name           = var.CW_LOG_STREAM_NAME
  log_group_name = aws_cloudwatch_log_group.reactapp_log_group.name
}