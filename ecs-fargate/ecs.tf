data "template_file" "image_template" {
  template = file("./templates/image.json")

  vars = {
    name           = var.CONTAINER_NAME
    image          = var.APP_IMAGE
    app_port       = var.HOST_PORT
    container_port = var.CONTAINER_PORT
  }
}

resource "aws_ecs_cluster" "main" {
  name = var.CLUSTER_NAME

  tags = {
    Name = var.TAG_NAME
  }
}

resource "aws_ecs_task_definition" "main" {
  family                   = "reactapp-task"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.CLUSTER_CPU
  memory                   = var.CLUSTER_MEMORY
  execution_role_arn       = aws_iam_role.ecs_task_exe_role.arn
  task_role_arn            = aws_iam_role.ecs_task_role.arn
  container_definitions    = data.template_file.image_template.rendered

  tags = {
    Name = var.TAG_NAME
  }
}


resource "aws_ecs_service" "main" {
  name                               = var.SERVICE_NAME
  cluster                            = aws_ecs_cluster.main.id
  task_definition                    = aws_ecs_task_definition.main.arn
  desired_count                      = var.SERVICE_DESIRED_COUNT
  deployment_minimum_healthy_percent = var.SERVICE_MIN_HEALTHY_PERCENT
  deployment_maximum_percent         = var.SERVICE_MAX_HEALTHY_PERCENT
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"

  network_configuration {
    security_groups  = [aws_security_group.SG_ECS.id]
    subnets          = aws_subnet.private.*.id
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.main.arn
    container_name   = var.CONTAINER_NAME
    container_port   = var.CONTAINER_PORT
  }

  lifecycle {
    ignore_changes = [task_definition, desired_count]
  }

  tags = {
    Name = var.TAG_NAME
  }

}