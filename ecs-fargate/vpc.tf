##Documents of the functions that using
#https://www.terraform.io/language/functions/element
#https://www.terraform.io/language/functions/length -> length of the list
#https://www.terraform.io/language/functions/compact -> remove empty elements
#count.index like foreach


resource "aws_vpc" "MAIN_VPC" {
  cidr_block = var.CIDR_BLOCK

  tags = {
    Name = var.TAG_NAME
  }
}

resource "aws_internet_gateway" "INTERNET_GATEWAY" {
  vpc_id = aws_vpc.MAIN_VPC.id

  tags = {
    Name = var.TAG_NAME
  }
}

resource "aws_subnet" "private" {
  vpc_id            = aws_vpc.MAIN_VPC.id
  cidr_block        = element(var.PRIVATE_SUBNETS, count.index)
  availability_zone = element(var.AVAILABILITY_ZONES, count.index)
  count             = length(var.PRIVATE_SUBNETS)
}

resource "aws_subnet" "public" {
  vpc_id                  = aws_vpc.MAIN_VPC.id
  cidr_block              = element(var.PUBLIC_SUBNETS, count.index)
  availability_zone       = element(var.AVAILABILITY_ZONES, count.index)
  count                   = length(var.PUBLIC_SUBNETS)
  map_public_ip_on_launch = true

  tags = {
    Name = var.TAG_NAME
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.MAIN_VPC.id

  tags = {
    Name = var.TAG_NAME
  }
}

resource "aws_route" "public" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.INTERNET_GATEWAY.id
}

resource "aws_route_table_association" "public" {
  count          = length(var.PUBLIC_SUBNETS)
  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public.id
}

resource "aws_nat_gateway" "main" {
  count         = length(var.PRIVATE_SUBNETS)
  allocation_id = element(aws_eip.nat.*.id, count.index)
  subnet_id     = element(aws_subnet.public.*.id, count.index)
  depends_on    = [aws_internet_gateway.INTERNET_GATEWAY]

  tags = {
    Name = var.TAG_NAME
  }
}

resource "aws_eip" "nat" {
  count = length(var.PRIVATE_SUBNETS)
  vpc   = true

  tags = {
    Name = var.TAG_NAME
  }
}

resource "aws_route_table" "private" {
  count  = length(var.PRIVATE_SUBNETS)
  vpc_id = aws_vpc.MAIN_VPC.id

  tags = {
    Name = var.TAG_NAME
  }
}

resource "aws_route" "private" {
  count                  = length(var.PRIVATE_SUBNETS)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.main.*.id, count.index)
  route_table_id         = element(aws_route_table.private.*.id, count.index)
}

resource "aws_route_table_association" "private" {
  count          = length(var.PRIVATE_SUBNETS)
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}