# **Protein & Patika.dev DevOps Engineer Bootcamp Graduation Project**

**Note 1** : This README file contains general informations. Each project directory has own README file.



## **Table of Contents**
---
- [Assignment 1](#assignment-1)

- [Assignment 2](#assignment-2--deploy-the-apptication-to-aws)

- [Assignment 3](#assignment-3-deploy-the-application-to-kubernetes)

- [Bonus](#bonus)

- [Files](#files)

- [Directories](#directories)

- [Build and Push Docker image to DockerHub](#build-and-push-docker-image-to-dockerhub)

- [GitLab Configurations](#gitlab-configurations)

- [GitLab Pipeline](#pipeline)

- [Videos (Run Projects and Pipeline)](#videos)


## **Assignments**
---

### **Assignment 1**
- Create a basic React app.
- Create a Dockerfile for the app.Try to form an image with the smallest possible size.
- Design and write a GitLab CI/CD pipeline featuring your Docker file for your brand new app.
  
### **Assignment 2 : Deploy the Apptication to AWS**
- Use Terraform to provision the infrastructure.
- Application should be running on ECS Fargate.
- Make sure to use the ideal VPC and Security Group setting.
- Application Load Balancer must be configured in front of the service.
- Feel free to include or use any other AWS service.

### **Assignment 3: Deploy the Application to Kubernetes**
- Write the required core manifest files.
- Explain the details in your project's README.Describe your strategy and choices. Also don't forget to write comments for necessary lines in the Docker file and GitLab YAML.
  
### **Bonus**

- Configure Auto Scaling onto your Fargate instance. Scale-up when CPU is above 50% and scale-down when CPU is below 20%.
- Create the proper CloudWatch dashboards and metrics for monitoring the performence of the application.
- Draw your AWS architecture, explain the connections and relations between your configured services.
- Instead of using GitLab's shared runners, you can configure your own runner. If you do so, pleae share the runner's config.toml and clarify the implementation process.
- Write a script (Ansible&Shell) that sends a notification by e-mail if the disk usage exceeds 90% in an operating system using any Linux distribution.


## **Files**
---
- **.gitlab-ci.yaml** : GitLab CI file. This file has 4 stages.
  - **"run-tests" stage**: It runs some tests. (It just write to stdout somethings.)
  - **"push-image-to-docker-hub" stage"**: Firstly it runs "docker login" command. Then build the image and push it to docker hub.
  - **"deploy-app-to-ecs" stage**: It updates the ECS Fargate service using "awscli".
  - **"deploy-app-to-kubernetes" stage**: It updates kubernetes cluster. 
- **Dockerfile** : It is Dockerfile for the application. It use 2 images. First one is node:alphine. We will copy all project files to work directory and build the application with npm command.Then we will use nginx image. We will copy nginx configuration file from ngnix directory to container. Then we will start the ngnix. 


## **Directories**
---
- **app** : It contains react app.
- **ansible** : It contains ansible playbook, hosts and script files that check to disk usage.
- **ecs-fargate**: It contains terraform script stack to deploy the application to ECS Fargate.
- **gitlab-runner**: It contains terraform script stack to create a gitlab runner server with EC2 instance.
- **kubernetes** : It contains kubernetes manifest files to deploy the application to kubernetes.
- **ngnix**: It contains ngnix configuration file.


## **Build and Push Docker image to Dockerhub**
---

This part contains commands to build docker image.I will push the image my dockerhub repository.

- Firstly, we need to login to docker.

` docker login -u "oguzbalkaya" -p "password" `

- We need to build the image:

` docker build -t oguzbalkaya/my_project:latest . `

- Push the image to docker

` docker push oguzbalkaya/my_project:latest `


## **GitLab Configurations**
---
We need to add variables to GitLab to run the projects correctly. Each project has own variables. 

**To add variables;**
- Go "Settings>CI/CD>Variables"
- Click "Add variable"

## - **Docker user informations for dockerlogin command**

- **CI_REGISTRY_USER**: DockerHub username.
- **CI_REGISTRY_PASSWORD**: DockerHub password.

## - **Repository and tag informations to build and push the image**
- **REPOSITORY**: Repository and tag. (e.g. oguzbalkaya/my_project:latest)
  
## - **Cluster and service information**
- **AWS_CLUSTER_NAME** : ECS Fargate cluster name
- **AWS_SERVICE_NAME** : ECS Fargate service name

## - **GitLab agent information**
- **KUBE_CONTEXT** : GitLab agent context. (e.g. oguzb/protein-devops-bootcamp-graduation-project:myagent)

## - **AWS user informations**

We need to add the variable to access AWS with awscli.We will use awscli to update ECS Fargate.

- **AWS_ACCESS_KEY_ID** : User access key id.
- **AWS_SECRET_ACCESS_KEY** : USer secret access key.

## - **Kubernetes informations**

- **DEPLOYMENT_NAME**: Kubernetes deployment name.
  
![GitLab Variables](images/variables.png)

## **Pipeline**
---

![Pipeline Jobs](images/pipeline.png)

Pipeline has 4 stage.

- **"run-tests" stage**: It runs some tests. (It just write to stdout somethings.)
- **"push-image-to-docker-hub" stage"**: Firstly it runs "docker login" command. Then build the image and push it to docker hub.
- **"deploy-app-to-ecs" stage**: It updates the ECS Fargate service using "awscli".
- **"deploy-app-to-kubernetes" stage**: It updates kubernetes cluster. 


## **Videos**

- Run Ansible [(click here to watch the video)](https://drive.google.com/file/d/1oE1NRMNsUXSUagbmyMSoWRyFP4AlzYnd/view?usp=sharing)
- Run ECS-Fargate [(click here to watch the video)](https://drive.google.com/file/d/1m92pAF2N_iGaBlZz5VQBDlasGdj3iVKA/view?usp=sharing)
- Run GitLab Runner [(click here to watch the video)](https://drive.google.com/file/d/1FkWHgEqo2oX3KcKtr2OVOb-BdAPoZS3w/view?usp=sharing)
- GitLab Pipeline [(click here to watch video)](https://drive.google.com/file/d/1BdYvF4uxA9kpAVXhu6D7msyduGyTckJr/view?usp=sharing)
