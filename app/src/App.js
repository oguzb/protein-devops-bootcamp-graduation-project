import logo from './devops.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Protein&Patika.dev DevOps Engineer Bootcamp Graduation Project<br></br>Oğuz BALKAYA
        </p>
        <a
          className="github_repo"
          href="https://github.com/oguzbalkaya"
          target="_blank"
          rel="noopener noreferrer"
        >
          Click to check out my GitHub repository
        </a>
      </header>
    </div>
  );
}

export default App;
