FROM node:alpine as build

WORKDIR /react_app

ENV PATH /app/node_modules/.bin:$PATH

COPY ./app /react_app

RUN npm run build

FROM nginx:alpine
COPY --from=build /react_app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
