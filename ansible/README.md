# **Check Status of Disks and Send Notification by Mail**

## **Table of Contents**
---

- [Requirements](#requirements)

- [What it does?](#what-it-does)

- [Files](#files)

- [Run](#run)

- [Technologies](#technologies)


## **Requirements**

-  **Pip** must be installed.You can find instructions of installation of Pip from [here](https://pip.pypa.io/en/stable/installation/).
-  **Ansible** must be installed.You can find instructions of installation of Ansible from [here](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).
- Write all host which you want to run the task to "hosts" file.

## **What it does?**

- Copy script files to remote hosts and give execution permission.
- Create a cronjob to run the script every minutes.
- The script checks disk usage.If disk usage is greater than 90%, send notification to admin by mail.

## **Files**

- **images/** : Screenshots for readme file.
- **script/**
    - **disk_alert.sh** : Main script file. It checks disks and if disk usage is greater than 90%, send notification to admin by mail.
    - **disk_alert.conf** : Configuration file of the script.It contains variables.
- **hosts** : Ansible inventory file. It contains remote hosts informations.
- **playbook.yaml** : Ansible playbook file. It contains tasks.


## **Run**

` ansible-playbook --inventory=hosts playbook.yaml `

![Run ansible-playbook](images/ansibleplaybook.png)

## **Technologies**

- Python Pip
- Ansible
- Linux
- Shell Script